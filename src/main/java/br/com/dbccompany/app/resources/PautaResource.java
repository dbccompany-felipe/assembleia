package br.com.dbccompany.app.resources;

import br.com.dbccompany.app.domains.dtos.InserePautaDTO;
import br.com.dbccompany.app.domains.dtos.PautaDTO;
import br.com.dbccompany.app.domains.dtos.ResultadoDTO;
import br.com.dbccompany.app.domains.dtos.VotoDTO;
import br.com.dbccompany.app.exceptions.ObjectNotFoundException;
import br.com.dbccompany.app.services.PautaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping(value = "/pauta/v1")
@Api(tags = "Pauta")
public class PautaResource {

    @Autowired
    private PautaService service;

    @ApiOperation(value = "Responsável por inserir uma pauta")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Retorna de sucesso padrão")
    })
    @PostMapping
    public ResponseEntity<String> inserir(
            @ApiParam(value = "Objeto que deve conter os dados necessários para inserir uma pauta", required = true)
            @Valid @RequestBody InserePautaDTO pauta) {
        Long id = service.inserir(pauta).getId();
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}").buildAndExpand(id)
                .toUri();
        return ResponseEntity.created(location).build();
    }

    @ApiOperation(value = "Serviço Responsável por buscar uma pauta pelo codigo identificador")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Retorno de sucesso para consulta"),
            @ApiResponse(code = 404, message = ObjectNotFoundException.PAUTA_NAO_ENCONTRADA)}
    )
    @GetMapping("/{id}")
    public ResponseEntity<PautaDTO> findById(@ApiParam(value = "Identificador da pauta", required = true)
            @PathVariable(value = "id") Long id){
        return ResponseEntity.ok(service.findById(id));
    }


    @ApiOperation(value = "Responsável por inserir um voto")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Retorna de sucesso padrão")
    })
    @PostMapping("/{id}/votar")
    public ResponseEntity<String> votar(
            @ApiParam(value = "Identificador da pauta", required = true)
            @PathVariable(value = "id") Long id,
            @ApiParam(value = "Objeto que deve conter as informações de voto", required = true)
            @Valid @RequestBody VotoDTO voto) {

        service.votar(id, voto);
        return ResponseEntity.status(201).build();
    }

    @ApiOperation(value = "Responsável por restornar o resultado de uma pauta")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Retorna de sucesso padrão")
    })
    @GetMapping("/{id}/resultado")
    public ResponseEntity<ResultadoDTO> getResultado(
            @ApiParam(value = "Identificador da pauta", required = true)
            @PathVariable(value = "id") Long id) {

        ResultadoDTO resultadoDTO = service.getResultado(id);
        return ResponseEntity.ok().body(resultadoDTO);
    }
}
