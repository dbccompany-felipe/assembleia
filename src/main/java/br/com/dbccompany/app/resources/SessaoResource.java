package br.com.dbccompany.app.resources;

import br.com.dbccompany.app.domains.dtos.AbrirSessaoDTO;
import br.com.dbccompany.app.services.SessaoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/sessao/v1")
@Api(tags = "Sessão")
public class SessaoResource {

    @Autowired
    private SessaoService service;

    @ApiOperation(value = "Responsável por Abrir uma sessão")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Retorna de sucesso padrão")
    })
    @PostMapping
    public ResponseEntity<String> inserir(
            @ApiParam(value = "Objeto que deve conter os dados necessários para abrir uma sessão", required = true)
            @Valid @RequestBody AbrirSessaoDTO abrirSessao) {
        service.abrirSessao(abrirSessao);
        return ResponseEntity.status(201).build();
    }
}
