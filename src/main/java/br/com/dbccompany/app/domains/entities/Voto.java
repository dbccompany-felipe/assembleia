package br.com.dbccompany.app.domains.entities;

import br.com.dbccompany.app.domains.enumerations.SimNao;
import br.com.dbccompany.app.domains.enumerations.SimNaoConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "votos", uniqueConstraints={
        @UniqueConstraint(columnNames = {"id_sessao", "cpf"})
})
public class Voto {

    @Id
    @SequenceGenerator(name = "seq_voto", sequenceName = "seq_voto", initialValue = 1, allocationSize= 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_voto")
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "voto", nullable = false)
    @Convert(converter = SimNaoConverter.class)
    private SimNao voto;

    @Column(name = "cpf", nullable = false)
    private String cpf;

    @Column(name = "data_hora", nullable = false)
    private LocalDateTime dataHora;

    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "id_sessao",nullable = false)
    private Sessao sessao;
}
