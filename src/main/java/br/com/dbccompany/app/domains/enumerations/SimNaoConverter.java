package br.com.dbccompany.app.domains.enumerations;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class SimNaoConverter implements AttributeConverter<SimNao, String> {

    @Override
    public String convertToDatabaseColumn(SimNao attribute) {
        if (attribute != null) {
            return attribute.getCode();
        }
        return null;
    }

    @Override
    public SimNao convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            return null;
        }
        for (SimNao x : SimNao.values()) {
            if (dbData.equalsIgnoreCase(x.getCode())) {
                return x;
            }
        }
        throw new IllegalArgumentException("Id inválido: " + dbData);
    }
}
