package br.com.dbccompany.app.domains.dtos;

import br.com.dbccompany.app.validations.ValidPauta;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "AbrirSessao", description = "Objeto responsável por Abrir uma sessão")
public class AbrirSessaoDTO {


    @ValidPauta
    @NotNull
    private Long idPauta;
    @ApiModelProperty(value = "Tempo em minutos da sessão aberta - default 1 min")
    private Long tempoSessaoAberta;

}
