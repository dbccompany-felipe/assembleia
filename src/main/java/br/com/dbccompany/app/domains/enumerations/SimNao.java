package br.com.dbccompany.app.domains.enumerations;

import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor
public enum SimNao {

    SIM("S"),
    NAO("N");
	   
    @Getter
    private String code;

}
