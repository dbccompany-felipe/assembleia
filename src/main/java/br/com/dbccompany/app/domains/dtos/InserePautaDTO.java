package br.com.dbccompany.app.domains.dtos;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@ApiModel(value = "InserePauta", description = "Objeto responsável por inserir uma pauta")
public class InserePautaDTO {

    @NotEmpty
    @ApiModelProperty(value = "Descrição da pauta", required = true)
    private String descricao;
}
