package br.com.dbccompany.app.domains.dtos;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "Resultado", description = "Objeto responsável por retornar o resultado de uma pauta")
public class ResultadoDTO {

    private PautaDTO pauta;
    private Long quantidadeSim;
    private Long quantidadeNao;
}
