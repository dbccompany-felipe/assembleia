package br.com.dbccompany.app.domains.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sessoes", uniqueConstraints={
        @UniqueConstraint(columnNames = {"id_pauta"})
})
public class Sessao {

    @Id
    @SequenceGenerator(name = "seq_sessao", sequenceName = "seq_sessao", initialValue = 1, allocationSize= 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_sessao")
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "data_hora_abertura", nullable = false)
    private LocalDateTime dataHoraAbertura;

    @Column(name = "tempo_sessao", nullable = false)
    private Long tempoSessao;

    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "id_pauta",nullable = false)
    private Pauta pauta;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy="sessao", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Voto> votos;
}
