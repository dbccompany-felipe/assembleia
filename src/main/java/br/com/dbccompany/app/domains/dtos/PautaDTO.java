package br.com.dbccompany.app.domains.dtos;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "Pauta", description = "Objeto responsável por retornar uma pauta")
public class PautaDTO extends InserePautaDTO {

    @ApiModelProperty(value = "Identificador unico da pauta", required = true)
    private Long id;
}
