package br.com.dbccompany.app.domains.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "pautas")
public class Pauta {

    @Id
    @SequenceGenerator(name = "seq_pauta", sequenceName = "seq_pauta", initialValue = 1, allocationSize= 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_pauta")
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "descricao", nullable = false)
    private String descricao;
}
