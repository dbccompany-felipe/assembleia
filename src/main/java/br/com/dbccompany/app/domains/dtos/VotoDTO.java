package br.com.dbccompany.app.domains.dtos;

import br.com.dbccompany.app.domains.enumerations.SimNao;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "Voto", description = "Objeto responsável por receber informações de voto")
public class VotoDTO {

    @NotEmpty
    @CPF
    private String cpf;
    @NotNull
    private SimNao voto;

}
