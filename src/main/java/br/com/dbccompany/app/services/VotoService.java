package br.com.dbccompany.app.services;

import br.com.dbccompany.app.domains.dtos.VotoDTO;
import br.com.dbccompany.app.domains.entities.Pauta;
import br.com.dbccompany.app.domains.entities.Sessao;
import br.com.dbccompany.app.domains.entities.Voto;
import br.com.dbccompany.app.domains.enumerations.SimNao;
import br.com.dbccompany.app.exceptions.BusinessException;
import br.com.dbccompany.app.integrations.clients.UserInfoClient;
import br.com.dbccompany.app.integrations.dtos.StatusUser;
import br.com.dbccompany.app.integrations.dtos.UserDTO;
import br.com.dbccompany.app.repositories.VotoRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@Slf4j
public class VotoService {

    @Autowired
    private VotoRepository repository;
    @Autowired
    private SessaoService sessaoService;

    @Autowired
    private UserInfoClient userInfoClient;

    public Voto votar(Pauta pauta, VotoDTO votoDTO) {
        Sessao sessao = sessaoService.getSessaoByPautaAberta(pauta);

        String cpfFormatado = votoDTO.getCpf().replace(".", "").replace("-", "");

        UserDTO userDTO = userInfoClient.getUserCpf(cpfFormatado);
        if (StatusUser.UNABLE_TO_VOTE.equals(userDTO.getStatus())) {
            throw new BusinessException(BusinessException.USUARIO_NAO_PODE_VOTAR);
        }

        ModelMapper modelMapper = new ModelMapper();
        Voto voto = modelMapper.map(votoDTO, Voto.class);
        voto.setSessao(sessao);
        voto.setCpf(cpfFormatado);
        voto.setDataHora(LocalDateTime.now());
        try {
            log.info("Voto: " + voto.toString());
            return repository.save(voto);
        } catch (DataIntegrityViolationException e) {
            throw new BusinessException(BusinessException.VOTO_JA_COMPUTADO);
        }
    }


    public Long getQuantidade(SimNao simNao, Pauta pauta) {
        Sessao sessao = sessaoService.getSessaoByPauta(pauta);
        Long quantidade = repository.countByVotoAndSessao(simNao, sessao);
        return quantidade;
    }
}
