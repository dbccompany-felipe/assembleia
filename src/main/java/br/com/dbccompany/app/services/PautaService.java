package br.com.dbccompany.app.services;

import br.com.dbccompany.app.domains.dtos.InserePautaDTO;
import br.com.dbccompany.app.domains.dtos.PautaDTO;
import br.com.dbccompany.app.domains.dtos.ResultadoDTO;
import br.com.dbccompany.app.domains.dtos.VotoDTO;
import br.com.dbccompany.app.domains.entities.Pauta;
import br.com.dbccompany.app.domains.enumerations.SimNao;
import br.com.dbccompany.app.exceptions.ObjectNotFoundException;
import br.com.dbccompany.app.repositories.PautaRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PautaService {

    @Autowired
    private PautaRepository repository;

    @Autowired
    private VotoService votoService;

    public Pauta inserir(InserePautaDTO pautaDTO) {
        ModelMapper modelMapper = new ModelMapper();
        return repository.save(modelMapper.map(pautaDTO, Pauta.class));
    }

    public PautaDTO findById(Long id) {
        Pauta pauta = getPauta(id);

        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(pauta, PautaDTO.class);
    }

    private Pauta getPauta(Long id) {
        return findByIdOptional(id)
                .orElseThrow(() -> new ObjectNotFoundException(ObjectNotFoundException.PAUTA_NAO_ENCONTRADA));
    }

    public Optional<Pauta> findByIdOptional(Long id) {
        return repository.findById(id);
    }

    public void votar(Long id, VotoDTO votoDTO) {
        Pauta pauta = getPauta(id);
        votoService.votar(pauta, votoDTO);
    }

    public ResultadoDTO getResultado(Long id) {
        Pauta pauta = getPauta(id);

        ModelMapper modelMapper = new ModelMapper();

        ResultadoDTO resultadoDTO = new ResultadoDTO();
        resultadoDTO.setQuantidadeSim(votoService.getQuantidade(SimNao.SIM, pauta));
        resultadoDTO.setQuantidadeNao(votoService.getQuantidade(SimNao.NAO, pauta));
        resultadoDTO.setPauta(modelMapper.map(pauta, PautaDTO.class));

        return resultadoDTO;
    }
}
