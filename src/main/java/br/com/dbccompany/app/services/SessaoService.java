package br.com.dbccompany.app.services;

import br.com.dbccompany.app.domains.dtos.AbrirSessaoDTO;
import br.com.dbccompany.app.domains.entities.Pauta;
import br.com.dbccompany.app.domains.entities.Sessao;
import br.com.dbccompany.app.exceptions.BusinessException;
import br.com.dbccompany.app.exceptions.ObjectNotFoundException;
import br.com.dbccompany.app.repositories.SessaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;

@Service
public class SessaoService {

    @Autowired
    private SessaoRepository repository;
    @Autowired
    private PautaService pautaService;

    public Sessao abrirSessao(AbrirSessaoDTO abrirSessao) {
        //Como ja é validada a entrada do idPauta, nao preciso fazer uma segunda validação
        //mas poderia
        Pauta pauta = pautaService.findByIdOptional(abrirSessao.getIdPauta()).get();

        try {
            Sessao sessao = new Sessao();
            sessao.setDataHoraAbertura(LocalDateTime.now());
            sessao.setTempoSessao(abrirSessao.getTempoSessaoAberta() == null ? 1 : abrirSessao.getTempoSessaoAberta());
            sessao.setPauta(pauta);
            return repository.save(sessao);
        } catch (DataIntegrityViolationException e) {
            throw new BusinessException(BusinessException.SESSAO_JA_ABERTA_PARA_ESSA_PAUTA);
        }
    }

    public Sessao getSessaoByPautaAberta(Pauta pauta) {
        Sessao sessao = getSessaoByPauta(pauta);

        LocalDateTime dataHoraAbertura = sessao.getDataHoraAbertura();
        long minutos = Duration.between(dataHoraAbertura, LocalDateTime.now()).toMinutes();
        if (minutos > sessao.getTempoSessao()) {
            throw new BusinessException(BusinessException.SESSAO_JA_FINALIZADA);
        }

        return sessao;
    }

    public Sessao getSessaoByPauta(Pauta pauta) {
        Sessao sessao = repository.findByPauta(pauta)
                .orElseThrow(() -> new ObjectNotFoundException(ObjectNotFoundException.SESSAO_NAO_ENCONTRADA));
        return sessao;
    }
}
