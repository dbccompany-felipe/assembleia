package br.com.dbccompany.app.integrations.clients;

import br.com.dbccompany.app.integrations.dtos.UserDTO;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class UserInfoFalbackFactory implements FallbackFactory<UserInfoClient> {

    @Override
    public UserInfoClient create(Throwable throwable) {
        return new UserInfoClient() {
            @Override
            public UserDTO getUserCpf(String cpf) {
                return null;
            }
        };
    }
}
