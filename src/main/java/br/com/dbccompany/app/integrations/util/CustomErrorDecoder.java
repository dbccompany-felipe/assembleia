package br.com.dbccompany.app.integrations.util;

import br.com.dbccompany.app.exceptions.ObjectNotFoundException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CustomErrorDecoder implements ErrorDecoder {
    private final ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String methodKey, Response response) {
        ObjectMapper mapper = new ObjectMapper();

        try {
            ErrorObject object = mapper.readValue(response.body().toString(), ErrorObject.class);
            switch (response.status()) {
//                case 400:
//                    return new BusinessServerException(object.getMessage());
                case 404:
                    return new ObjectNotFoundException(object.getMessage());
                case 503:
//                    return new IndisponivelException(IndisponivelException.NAO_DISPONIVEL);
//                case 500:
//                    return new IntegrationServerException(object.getMessage());
                default:
                    return new Exception("Generic error");
            }
        } catch (JsonProcessingException e) {
            return new Exception(e);
        }
    }
}