package br.com.dbccompany.app.integrations.dtos;

import lombok.Data;

@Data
public class UserDTO {

    private StatusUser status;
}
