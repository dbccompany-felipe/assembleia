package br.com.dbccompany.app.integrations.clients;

import br.com.dbccompany.app.integrations.configs.IntegrationConfiguration;
import br.com.dbccompany.app.integrations.dtos.UserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "user-info", url = "${integration.user-info.url}", configuration = IntegrationConfiguration.class,
    fallbackFactory = UserInfoFalbackFactory.class)
public interface UserInfoClient {

    @GetMapping(value = "/users/{cpf}")
    UserDTO getUserCpf(@PathVariable(value = "cpf") String cpf);

}
