package br.com.dbccompany.app.integrations.dtos;

public enum StatusUser {

    ABLE_TO_VOTE, UNABLE_TO_VOTE
}
