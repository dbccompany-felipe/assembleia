package br.com.dbccompany.app.integrations.configs;

import br.com.dbccompany.app.integrations.util.CustomErrorDecoder;
import br.com.dbccompany.app.integrations.util.CustomRetryer;
import feign.Retryer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

public class IntegrationConfiguration {

    @Value("${integration.retryer}")
    private String retryer;

    @Bean
    public CustomErrorDecoder customErrorDecoder() {
        return new CustomErrorDecoder();
    }

    @Bean
    public Retryer retryer() {
        return new CustomRetryer(2000, Integer.valueOf(retryer));
    }


}
