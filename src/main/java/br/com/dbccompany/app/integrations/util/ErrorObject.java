package br.com.dbccompany.app.integrations.util;

import lombok.Data;

@Data
public class ErrorObject {

    private String timeStamp;
    private Integer status;
    private String path;
    private String error;
    private String message;
}
