package br.com.dbccompany.app.validations;

import br.com.dbccompany.app.domains.entities.Pauta;
import br.com.dbccompany.app.exceptions.ObjectNotFoundException;
import br.com.dbccompany.app.services.PautaService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

public class PautaValidator implements ConstraintValidator<ValidPauta, Long>  {

    @Autowired
    private PautaService pautaService;

    @Override
    public void initialize(ValidPauta ann) {
    }

    @Override
    public boolean isValid(Long aLong, ConstraintValidatorContext context) {
        if (aLong != null) {
            Optional<Pauta> pauta = pautaService.findByIdOptional(aLong);
            if (!pauta.isPresent()) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate(ObjectNotFoundException.PAUTA_NAO_ENCONTRADA)
                        .addConstraintViolation();
                return false;
            }
        }

        return true;
    }
}
