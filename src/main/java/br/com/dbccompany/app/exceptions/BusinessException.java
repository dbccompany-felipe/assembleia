package br.com.dbccompany.app.exceptions;

public class BusinessException extends RuntimeException {


    //Uso estrutura de key message para facilitar o frontend de usar alguma especie de tradução para multi-language
    public static String SESSAO_JA_ABERTA_PARA_ESSA_PAUTA = "msg_sessao_ja_aberta_para_essa_pauta";
    public static String SESSAO_JA_FINALIZADA = "msg_sessao_ja_finalizada";
    public static String VOTO_JA_COMPUTADO = "msg_voto_ja_computado";
    public static String USUARIO_NAO_PODE_VOTAR = "msg_usuario_nao_pode_votar";

    public BusinessException(String message) {
        super(message);
    }
}
