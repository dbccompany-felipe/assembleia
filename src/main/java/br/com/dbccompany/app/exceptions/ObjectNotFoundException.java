package br.com.dbccompany.app.exceptions;

public class ObjectNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

	public static final String PAUTA_NAO_ENCONTRADA = "msg_pauta_nao_encontrada";
	public static final String SESSAO_NAO_ENCONTRADA = "msg_sessao_nao_encontrada";

	public ObjectNotFoundException(String msg) {
		super(msg);
	}

	public ObjectNotFoundException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
