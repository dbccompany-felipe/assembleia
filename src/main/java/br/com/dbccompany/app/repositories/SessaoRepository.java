package br.com.dbccompany.app.repositories;

import br.com.dbccompany.app.domains.entities.Pauta;
import br.com.dbccompany.app.domains.entities.Sessao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SessaoRepository extends JpaRepository<Sessao, Long> {

    Optional<Sessao> findByPauta(Pauta pauta);
}
