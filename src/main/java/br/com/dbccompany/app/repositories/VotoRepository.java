package br.com.dbccompany.app.repositories;

import br.com.dbccompany.app.domains.entities.Sessao;
import br.com.dbccompany.app.domains.entities.Voto;
import br.com.dbccompany.app.domains.enumerations.SimNao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VotoRepository extends JpaRepository<Voto, Long> {

    Long countByVotoAndSessao(SimNao simNao, Sessao sessao);
}
