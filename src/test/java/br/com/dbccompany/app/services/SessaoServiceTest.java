package br.com.dbccompany.app.services;

import br.com.dbccompany.app.domains.dtos.AbrirSessaoDTO;
import br.com.dbccompany.app.domains.entities.Pauta;
import br.com.dbccompany.app.domains.entities.Sessao;
import br.com.dbccompany.app.exceptions.BusinessException;
import br.com.dbccompany.app.repositories.SessaoRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SessaoServiceTest {

    @Rule
    public ErrorCollector error = new ErrorCollector();

    @InjectMocks
    private SessaoService service = new SessaoService();

    @Mock
    private SessaoRepository repository;
    @Mock
    private PautaService pautaService;


    @Test
    public void saveTest() {
        Pauta pauta = new Pauta();
        pauta.setDescricao("Descricao");
        pauta.setId(1l);
        when(pautaService.findByIdOptional(any(Long.class))).thenReturn(Optional.of(pauta));

        AbrirSessaoDTO abrirSessaoDTO = new AbrirSessaoDTO();
        abrirSessaoDTO.setIdPauta(1l);

        when(repository.save(any(Sessao.class))).thenAnswer(new Answer<Sessao>() {
            @Override
            public Sessao answer(InvocationOnMock invocationOnMock) throws Throwable {
                Object[] args = invocationOnMock.getArguments();
                Sessao p = (Sessao) args[0];

                p.setId(1l);
                return p;
            }
        });

        Sessao sessao = service.abrirSessao(abrirSessaoDTO);

        error.checkThat(sessao.getDataHoraAbertura(), is(notNullValue()));
        error.checkThat(sessao.getTempoSessao(), equalTo(1l));
    }

    @Test
    public void saveDataIntegrity() {
        Pauta pauta = new Pauta();
        pauta.setDescricao("Descricao");
        pauta.setId(1l);
        when(pautaService.findByIdOptional(any(Long.class))).thenReturn(Optional.of(pauta));

        AbrirSessaoDTO abrirSessaoDTO = new AbrirSessaoDTO();
        abrirSessaoDTO.setIdPauta(1l);

        when(repository.save(any(Sessao.class))).thenThrow(new DataIntegrityViolationException("teste"));
        try {
            service.abrirSessao(abrirSessaoDTO);
            fail();
        } catch (BusinessException e) {
            assertEquals(e.getMessage(), BusinessException.SESSAO_JA_ABERTA_PARA_ESSA_PAUTA);
        }
    }
}
