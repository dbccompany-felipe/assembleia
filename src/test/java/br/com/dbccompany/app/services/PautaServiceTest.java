package br.com.dbccompany.app.services;

import br.com.dbccompany.app.domains.dtos.InserePautaDTO;
import br.com.dbccompany.app.domains.dtos.PautaDTO;
import br.com.dbccompany.app.domains.entities.Pauta;
import br.com.dbccompany.app.exceptions.ObjectNotFoundException;
import br.com.dbccompany.app.repositories.PautaRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class PautaServiceTest {

    @Rule
    public ErrorCollector error = new ErrorCollector();

    @InjectMocks
    private PautaService service = new PautaService();

    @Mock
    private PautaRepository repository;

    @Test
    public void saveTest() {
        InserePautaDTO pautaDTO = new PautaDTO();
        pautaDTO.setDescricao("Descricao");

        when(repository.save(any(Pauta.class))).thenAnswer(new Answer<Pauta>() {
            @Override
            public Pauta answer(InvocationOnMock invocationOnMock) throws Throwable {
                Object[] args = invocationOnMock.getArguments();
                Pauta p = (Pauta) args[0];
                p.setId(1l);
                return p;
            }
        });

        Pauta pauta = service.inserir(pautaDTO);
        assertEquals(pauta.getDescricao(), pautaDTO.getDescricao());
    }

    @Test
    public void findByIdTest() {
        Pauta pauta = new Pauta();
        pauta.setDescricao("Descricao");
        pauta.setId(1l);

        when(repository.findById(1l)).thenReturn(Optional.of(pauta));

        PautaDTO pautaDTO = service.findById(1l);

        error.checkThat(pautaDTO.getId(), equalTo(pauta.getId()));
        error.checkThat(pautaDTO.getDescricao(), equalTo(pauta.getDescricao()));
    }


    @Test
    public void findByIdNotFoundTest() {

        when(repository.findById(1l)).thenReturn(Optional.ofNullable(null));
        try {
            service.findById(1l);
            fail();
        } catch (ObjectNotFoundException e) {
            assertEquals(e.getMessage(), ObjectNotFoundException.PAUTA_NAO_ENCONTRADA);
        }
    }

}
